import {useState, useEffect} from 'react';
import {Link, useParams} from 'react-router-dom';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';

function ProductDetails() {

	const [image, setImage] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const {productId} = useParams();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_TINDERELLA_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data => {
			setImage(data.image);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	return(
		<Container id="background" fluid>
			<Container className="pb-5">
				<h1 className="text-center py-5">Product Details</h1>
				<Card>
					<Container fluid>
						<Row>
							<Col className="p-3">
								<Card.Img variant="top" src={image}/>
							</Col>
							<Col className="d-flex align-items-center p-0">
								<Card.Body>
								  <Card.Text as={Link} to={"/products"}>Back to Products</Card.Text>
								  <Card.Title className="mb-3">{name}</Card.Title>
								  <Card.Subtitle>Description:</Card.Subtitle>
								  <Card.Text>{description}</Card.Text>
								  <Card.Subtitle>Price:</Card.Subtitle>
								  <Card.Text>Php {price}</Card.Text>
								  <Button className="me-1" as={Link} to={`/order/${productId}`}>Order</Button>
								  <Button>Add to Cart</Button>
								</Card.Body>
							</Col>
						</Row>
					</Container>
				</Card>
			</Container>
		</Container>
	)
}

export default ProductDetails;