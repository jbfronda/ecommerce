import {Container, Navbar, Nav, Image} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import {useContext, Fragment} from 'react';
import UserContext from '../../UserContext.js';
import './NavBar.css';

function NavBar() {

  const {user} = useContext(UserContext);

  return (
    <Container id="navbar" fluid>
      <Navbar expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/"><Image src="https://res.cloudinary.com/dm2aaqucx/image/upload/v1676769339/Tinderella_NavBrand.png" id="navbar-brand" fluid/></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav id="nav-links" className="ms-auto">
              <Nav.Link as={NavLink} to="/">Home</Nav.Link>
              <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
              {
                user ?
                  
                    user.isAdmin ?
                    <Fragment>
                      <Nav.Link as = {NavLink} to = "/adminDashboard">Admin Dashboard</Nav.Link>
                      <Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
                    </Fragment>
                    :
                    <Fragment>
                      <Nav.Link as = {NavLink} to = "/my-orders">My orders</Nav.Link>
                      <Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
                    </Fragment>
                  
                :
                <Fragment>
                  <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                </Fragment>
              }
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </Container>
  );
}

export default NavBar;