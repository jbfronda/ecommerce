import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

function AdminDashboardTable({prop}) {

  const {_id, name, description, price, isActive} = prop;

  return (
    <tr>
      <td>{name}</td>
      <td>{description}</td>
      <td>{price}</td>
      {
        isActive?
        <td>Available</td>
        :
        <td>Not Available</td>
      }
      <td className="d-flex flex-column align-items-center">
          <Button className="col-8 mb-1" variant="primary" as={Link} to={`/product/update/${_id}`}>Update</Button>
          <Button className="col-8" variant="primary" as={Link} to={`/product/archive/${_id}`}>Status</Button>
      </td>
    </tr>
  );
}

export default AdminDashboardTable;