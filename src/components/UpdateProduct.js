import {useState, useEffect} from 'react';
import {Container, Card, Button, Form} from 'react-bootstrap';
import {Link, useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

function UpdateProduct() {

	const [image, setImage] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const {productId} = useParams();
	const navigate = useNavigate();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_TINDERELLA_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data => {
			setImage(data.image);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	const updateProduct = (event) => {
	  event.preventDefault();
	  fetch(`${process.env.REACT_APP_TINDERELLA_URL}/product/update/${productId}`, {
	    method: 'PUT',
	    headers: {
	      Authorization: `Bearer ${localStorage.getItem('token')}`,
	      'Content-Type': 'application/json'
	    },
	    body: JSON.stringify({
	      image: image,
	      name: name,
	      description: description,
	      price: price
	    })
	  })
	  .then(result => result.json())
	  .then(data => {

	    if(data === false){
	      Swal.fire({
            title: "Operation failed!",
            icon: "error",
            text: "Please try again!"
          })
	    }else{
	    	Swal.fire({
	    	  title: "Product updated!",
	    	  icon: "success",
	    	  text: "Back to Dashboard"
	    	})
	      navigate('/adminDashboard');
	    }
	  })
	};

	return(
		<Container>
			<h1 className="text-center my-5">Update Product</h1>
			<Card className="col-md-6 mx-auto p-5">
				<Form onSubmit = {event => updateProduct(event)}>
		          <Form.Group className="mb-3" controlId="formBasicProduct">
		            <Form.Label>Product Name</Form.Label>
		            <Form.Control
		              type="string"
		              placeholder="Product Name"
		              value={name}
		              onChange={event => setName(event.target.value)}
		              required
		            />
		          </Form.Group>

		          <Form.Group className="mb-3" controlId="formBasicPrice">
		            <Form.Label>Price</Form.Label>
		            <Form.Control
		              type="string"
		              placeholder="Price"
		              value={price}
		              onChange={event => setPrice(event.target.value)}
		              required
		            />
		          </Form.Group>

		          <Form.Group className="mb-3" controlId="formBasicDescription">
		            <Form.Label>Description</Form.Label>
		            <Form.Control
		              as="textarea" rows={3}
		              type="string"
		              placeholder="Description"
		              value={description}
		              onChange={event => setDescription(event.target.value)}
		              required
		            />
		          </Form.Group>

		          <Form.Group className="mb-5" controlId="formBasicImage">
		            <Form.Label>Image URL</Form.Label>
		            <Form.Control
		              type="string"
		              placeholder="Image URL"
		              value={image}
		              onChange={event => setImage(event.target.value)}
		              required
		            />
		          </Form.Group>

		          <Button className="me-1" variant="primary" as={Link} to={"/adminDashboard"}>
		           	Go back
		           </Button>
		           <Button variant="primary" type="submit">
		           	Update
		           </Button>
		        </Form>
	        </Card>
		</Container>
	)
};

export default UpdateProduct;