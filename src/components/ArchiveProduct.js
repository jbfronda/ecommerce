import React, {useState} from "react";
import {Container, Form, Button} from "react-bootstrap";
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

const ArchiveProduct = () => {
  const [item, setItem] = useState({ kindOfStand: "", another: "another" });
  const {kindOfStand} = item;
  const {productId} = useParams();
  const navigate = useNavigate();

  const handleChange = event => {
    event.persist();

    setItem(prevState => ({
      ...prevState,
      kindOfStand: event.target.value
    }));
  };

  const archiveProduct = event => {
    event.preventDefault();
      fetch(`${process.env.REACT_APP_TINDERELLA_URL}/product/archive/${productId}`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          isActive: `${kindOfStand}`
        })
      })
      .then(result => result.json())
      .then(data => {

        if(data === false){
          Swal.fire({
            title: "Operation failed!",
            icon: "error",
            text: "Please try again!"
          })
        }else{
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "Product status updated!"
          })
          navigate('/adminDashboard');
        }
      })
    };

  return (
    <Container className="text-center d-flex flex-column align-items-center">
      <h1>Archive Product</h1>
      <Form onSubmit={event => archiveProduct(event)}>
        <Form.Group controlId="kindOfStand">
          <Form.Check
            value={true}
            type="radio"
            aria-label="radio 1"
            label="Enable"
            onChange={handleChange}
            checked={kindOfStand === "true"}
          />
          <Form.Check
            value={false}
            type="radio"
            aria-label="radio 2"
            label="Disable"
            onChange={handleChange}
            checked={kindOfStand === "false"}
          />
        </Form.Group>
        <Button className="mt-3" variant="primary" type="submit">
          Update
        </Button>
      </Form>
    </Container>
  );
};

export default ArchiveProduct;