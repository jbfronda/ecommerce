function MyOrdersTable({prop}){

	const {username, name, description, price, quantity, totalAmount, purchasedOn} = prop;

	return(
		<tr>
			<td>{name}</td>
			<td>{description}</td>
			<td>{price}</td>
			<td>{quantity}</td>
			<td>{totalAmount}</td>
			<td>{purchasedOn}</td>
		</tr>
	)
}

export default MyOrdersTable;