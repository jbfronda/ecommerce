import {Container} from 'react-bootstrap';
import Banner from '../components/Banner.js';

function Home() {
  return(
  	<Container id="background" fluid>
      <Banner />
  	</Container>
  )
}

export default Home;