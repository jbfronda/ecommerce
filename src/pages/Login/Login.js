import {useState, useEffect, useContext} from 'react';
import {Container, Button, Form, Card} from 'react-bootstrap';
import {Link, Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../../UserContext.js';
import Swal from 'sweetalert2';
import './Login.css';

function Login() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const {user, setUser} = useContext(UserContext);
  const [isActive, setIsActive] = useState(false);

  useEffect(()=>{
    if(email !== "" && password !== ""){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }, [email, password]);

  const login = (event) => {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_TINDERELLA_URL}/user/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(result => result.json())
    .then(data => {
      console.log(data);

      if(data === false){
        Swal.fire({
          title: "Login failed!",
          icon: "error",
          text: "Please try again!"
        })
      }else{
        localStorage.setItem('token', data.auth);
        retrieveUserDetails(localStorage.getItem('token'));
        Swal.fire({
          title: "Login successful!",
          icon: 'success',
          text: "Welcome to our website!"
        })
        navigate('/');
      }
    })
  };

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_TINDERELLA_URL}/user/details`, {
      headers:{
          Authorization: `Bearer ${token}`
      }
    })
    .then(result => result.json())
    .then(data => {
      console.log(data)

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  };

  return (
    user ?
    <Navigate to ="*"/>
    :
    <Container id="container-login" fluid>
      <Container>
        <Card className="col-12 col-md-8 offset-md-2 col-lg-4 offset-lg-7 my-5">
          <Form className="p-5" onSubmit = {event => login(event)}>
            <h3 className="text-center mb-5">Login</h3>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email:</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={event => setEmail(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-4" controlId="formBasicPassword">
              <Form.Label>Password:</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={event => setPassword(event.target.value)}
                required
              />
            </Form.Group>

            {
              isActive?
              <Button id="login-button" type="submit" className="mb-5">
                Login
              </Button>
              :
              <Button id="login-button" type="submit" className="mb-5" disabled>
                Login
              </Button>
            }

            <Form.Group className="text-center" controlId="formBasicEmail">
              <Form.Label>Don't have an account yet? <Link to={"/register"}>Register</Link></Form.Label>
            </Form.Group>
          </Form>
        </Card>
      </Container>
    </Container>
  );
}

export default Login;