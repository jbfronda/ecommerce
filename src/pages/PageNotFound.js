import {Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function PageNotFound(){
	return(
		<Container id="background" fluid>
			<h1>Page Not Found</h1>
			<p>Go back to the <Link to="/">homepage</Link>.</p>
		</Container>
	)
}