import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

function Logout(){
	const {setUser, unSetUser} = useContext(UserContext);

	useEffect(() => {
		unSetUser();
		setUser(null);
		Swal.fire({
		  title: "Log out successful!",
		  icon: 'success'
		})
	}, []);
	

	return(
		<Navigate to = "/login"/>
	)
};

export default Logout;